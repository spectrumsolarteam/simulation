"""
Solar boat simulation base

!! NOTE !!
All distances (so including heights) are integers, representing centimeters (cm)
And so surfaces are measures in square centimeter (cm^2)
All angles are integers as well, so representing the rather odd decidegrees (d°). E.g. 1.8° = 18d° (factor 10)
Times are also integers; in milli seconds (ms)
"""
from math import *
import numpy as np
from random import randint

from .foils.foil import Foil

class Solarboat():
    STRUT_HEIGHT = 120
    
    height_port = 0
    height_starboard = 0
    height_stern = 0
    velocity = 0

    def __init__(self, 
                    foil_port: Foil = Foil,
                    foil_starboard: Foil  = Foil,
                    foil_stern: Foil  = Foil,
                    mass: int = 175,
                    time_step: int = 100):
        self.foil_port = foil_port
        self.foil_starboard = foil_starboard
        self.foil_stern = foil_stern

        self.mass = mass
        self.time_step = time_step
        self.set_sizes()

    def step(self):
        self.change_heights(
            delta_port      = self.calculate_delta_height(self.foil_port) * 100,
            delta_starboard = self.calculate_delta_height(self.foil_starboard) * 100,
            delta_stern     = self.calculate_delta_height(self.foil_stern) * 100,
        )

        return self.get_state()

    def calculate_delta_height(self, foil: Foil):
        net_force = self.calculate_netto_force(foil)
        return 0.5 * net_force/(self.mass/3.) * (self.time_step/1000) ** 2

    def calculate_netto_force(self, foil: Foil):
        return foil.calculate_lift(self.velocity) - self.mass * 9.81 / 3

    def change_foil_angles(self, delta_angle_port: int, delta_angle_starboard: int, delta_angle_stern: int):
        self.foil_port.change_angle(delta_angle_port)
        self.foil_starboard.change_angle(delta_angle_starboard)
        self.foil_stern.change_angle(delta_angle_stern)

    def get_state(self):
        roll,pitch = self.get_orientation()
        h_cg = self.get_cg_height()
        return [
            float(max(0, self.height_port + randint(-3, 3))), # noise applies here
            float(max(0, self.height_starboard + randint(-3, 3))), # noise applies here
            float(max(0, self.height_stern + randint(-3, 3))), # noise applies here
            float(roll),
            float(pitch),
            float(self.velocity),
            float(self.foil_port.angle),
            float(self.foil_starboard.angle),
            float(self.foil_stern.angle),
            float(max(0, h_cg + randint(-3, 3))), # noise applies here
            float(h_cg)
        ]

    def set_velocity(self, v: float):
        self.velocity = v

    def change_velocity(self, delta_v: float):
        self.velocity += delta_v

    def set_sizes(self, 
            transverse_distance_cg_to_front_foils: int = 70,
            longitudinal_distance_cg_to_front_foils: int = 70,
            longitudinal_distance_cg_to_stern_foil: int = 300):

        # longitudinal axis is from bow to stern
        # transverse axis is from port to starboard
        self.transverse_distance_cg_to_front_foils = transverse_distance_cg_to_front_foils
        self.longitudinal_distance_cg_to_front_foils = longitudinal_distance_cg_to_front_foils
        self.longitudinal_distance_cg_to_stern_foil = longitudinal_distance_cg_to_stern_foil

    def change_heights(self, delta_port = None, delta_starboard = None, delta_stern = None):
        if delta_port is not None:
            new_height = round(self.height_port + delta_port)
            self.height_port = max(0, min(self.STRUT_HEIGHT, new_height))
        if delta_starboard is not None:
            new_height = round(self.height_starboard + delta_starboard)
            self.height_starboard = max(0, min(self.STRUT_HEIGHT, new_height))
        if delta_stern is not None:
            new_height = round(self.height_stern + delta_stern)
            self.height_stern = max(0, min(self.STRUT_HEIGHT, new_height))

    def get_orientation(self):
        roll_angle = np.degrees(np.tan(((self.height_port - self.height_starboard) / 2.) / self.transverse_distance_cg_to_front_foils))
        pitch_angle = np.degrees(np.tan((self.get_cg_height() - self.height_stern) / self.longitudinal_distance_cg_to_stern_foil))

        return round(roll_angle*10), round(pitch_angle*10)

    def get_cg_height(self):
        # cg => center of gravity
        h_front = (self.height_port + self.height_starboard)/2.
        return (self.longitudinal_distance_cg_to_front_foils * h_front + self.longitudinal_distance_cg_to_stern_foil * self.height_stern) / (self.longitudinal_distance_cg_to_front_foils + self.longitudinal_distance_cg_to_stern_foil)

    def reset(self):
        self.height_port = 0
        self.height_starboard = 0
        self.height_stern = 0
        self.foil_port.angle = 0
        self.foil_starboard.angle = 0
        self.foil_stern.angle = 0