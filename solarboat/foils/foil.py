"""
Foil (base) class representing the actual foil, but not the strut!
To be inherited to create foil-specific behaviour such as C_L constants

!! NOTE !!
All distances (so including heights) are integers, representing centimeters (cm)
And so surfaces are measures in square centimeter (cm^2)
All angles are integers as well, so representing the rather odd decidegrees (d°). E.g. 1.8° = 18d° (factor 10)
Times are also integers; in milli seconds (ms)
"""
class Foil:
    RHO = 1025      # Density of sea water in kg/m3 (constant)
    angle = 0       # angle of attack in d° (abbreviated for convenience)

    def __init__(self,
                    surface_area: int,
                    min_angle: int,
                    max_angle: int) -> None:
        self.surface_area = surface_area
        self.max_angle = max_angle
        self.min_angle = min_angle

    def set_angle(self, a):
        if a < self.min_angle:
            self.angle = self.min_angle
        elif a > self.max_angle:
            self.angle = self.max_angle
        else:
            self.angle = a

    def change_angle(self, delta_a):
        self.set_angle(self.angle + delta_a)

    def calculate_lift(self, velocity) -> float:
        cl = self.get_cl(velocity)
        lift = cl * self.RHO * (velocity ** 2) * (self.surface_area / 10000) * 0.5
        return lift

    def get_cl(self, velocity) -> float:
        return 0