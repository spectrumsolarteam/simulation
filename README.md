# README #
This is the start of a simulation of a solarboat in Python, to be able to calculate on it.

It currently supports:

* hydrofoils (controlled by angle of attack)

### More coming soon
We're currently in the process of adding more documentation and adding more features!

### Contribute
We encourage you to extend the simulation with useful additions and we're **very happy** to accept pull requests. If you are not sure on how to extend this simulation yourself, you can always send us an email with your ideas: info@spectrumsolarteam.nl.

Contributing can be done in many ways, for example (but not limited to):

* Make setup more advanced (less 'harcoded' values / assumptions)
* Add your own foil mechanics
* Include more 'real world' physics
* Add new parts, such as motors
* Add more detailed noise, e.g. simulating waves
* Visualization (either 2D or 3D)
* ... any idea is welcome!

### License
This project is licensed under GNU GPLv3.