import os
from numpy.lib.function_base import angle
from .foil import Foil
from csv import reader

class NACA0012(Foil):
    def __init__(self,
                    surface_area: int,
                    min_angle: int,
                    max_angle: int) -> None:
        super().__init__(surface_area, min_angle, max_angle)
        self.import_csv()

    def import_csv(self) -> None:
        self.cl_list = {}
        with open(os.path.join(os.path.dirname(__file__), 'naca0012.csv'), 'r') as read_obj:
            csv_reader = reader(read_obj)
            for row in csv_reader:
                self.cl_list[row[0]] = float(row[1])


    def get_cl(self, v) -> float:
        angle_str = '%.1f' % (self.angle / 10)
        return self.cl_list[angle_str]
