from solarboat.foils.naca0012 import NACA0012
from solarboat.solarboat import Solarboat

from simple_pid import PID
import matplotlib.pyplot as plt

"""
This is an example, where a PID controller controls the foil angle. The goal (setpoint) is set to 50 cm. It runs 300 steps (with dt=200ms, thus 60 secs)

It uses matplotlib and simple_pid, you should install these to be able to run the example.
"""

boat = Solarboat(
    foil_port=NACA0012(450, 0, 70),
    foil_starboard=NACA0012(450, 0, 70),
    foil_stern=NACA0012(600, 0, 70),
    mass=170,
    time_step=200
)
boat.set_velocity(7)

pid_port = PID(0.15, 0.1, 0.06, setpoint=50)
pid_starboard = PID(0.15, 0.1, 0.06, setpoint=50)
pid_stern  = PID(0.25, 0.1, 0.06, setpoint=50)

pid_port.output_limits = (0, 70)
pid_starboard.output_limits = (0, 70)
pid_stern.output_limits = (0, 70)

observations = []
observation = boat.get_state()
for i in range(300):
    height_port = observation[0]
    height_starboard = observation[1]
    height_stern = observation[2]

    angle_port = pid_port(height_port, dt=0.2)
    angle_starboard = pid_starboard(height_starboard, dt=0.2)
    angle_stern = pid_stern(height_stern, dt=0.2)

    boat.change_foil_angles(
        angle_port - boat.foil_port.angle,
        angle_starboard - boat.foil_starboard.angle,
        angle_stern - boat.foil_stern.angle
    )

    observation = boat.step()
    observations.append(observation)


# make a simple plot
t = [(i*200)/1000 for i in range(len(observations))]
roll_data = [x[3]/10 for x in observations]
pitch_data = [x[4]/10 for x in observations]
cg_real_height_data = [x[10] for x in observations]

fig, ax1 = plt.subplots()

ax1.set_xlabel('time (s)')
ax1.set_ylabel('Degrees')
ax1.plot(t, roll_data, color='tab:red', label="Roll (deg)")
ax1.plot(t, pitch_data, color='tab:blue', label="Pitch (deg)")

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

ax2.set_ylabel('Centimeters')  # we already handled the x-label with ax1
ax2.plot(t, cg_real_height_data, color='tab:olive', label="Height (cm)")

handles, labels = [(a + b) for a, b in zip(ax1.get_legend_handles_labels(), ax2.get_legend_handles_labels())]
plt.legend(handles, labels)

fig.set_size_inches(12,6)
plt.show()